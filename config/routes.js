const database = require("../middlewares/database");
const tracks = require('../app/controllers/tracks');
const imageUploader = require('../app/controllers/imageUploader');
const auth = require("../app/controllers/auth");
const tokenmiddleware = require("../middlewares/tokenmiddleware");
const express = require('express');


class Routes {
  constructor(app) {

    new database.Database(app)
    new auth.Auth(app).routes()
    new tokenmiddleware.TokenMiddleware(app)
    new tracks.Tracks(app).routes()
    new imageUploader.ImageUploader(app).routes()

    // serve static images
    app.use('/images', express.static('uploads'));

    app.get("/", (req, res) => {
      res.json({
        msg: "Xamk API!",
      });
    });
    app.get("*", (req, res) => {
      res.status(404).json({
        msg: "Route not found",
      });
    })
  }
}

exports.Routes = Routes