module.exports = {
  apps : [
    {
      name: "Xamk",
      script: "./app.js",
      watch: true,
      env: {
        "PORT": 3000,
        "NODE_ENV": "development"
      },
      env_production: {
        "PORT": 3007,
        "NODE_ENV": "production",
      }
    }
  ]
}
