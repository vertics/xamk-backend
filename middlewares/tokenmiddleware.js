const jwt = require('jsonwebtoken');

class TokenMiddleware {
  
  constructor(app) {
    app.use(async (req, res, next) => {

      if (req.method === 'POST' || req.method === 'PUT' || req.method === 'PATCH' || req.method === 'DELETE' ) {
        let token = req.headers['x-access-token'];

        if (!token) {
      
          return res.status(403).send({ auth: false, message: 'No token provided.' });
        }
        else {
          jwt.verify(token, process.env.secret_key, function(err, decoded) {
            if (err) {
              // console.log("ERROR:" + err)
              return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
      
            }
            else {
                  // if everything good, save to request for use in other routes
              req.userId = decoded.id;
              next()
            }
         })

      }
      } else {

       next()
      
      }
  })
} 

}
exports.TokenMiddleware = TokenMiddleware;

