const mongoose = require("mongoose");
class Database {
  constructor(app) {
    app.use((req, res, next) => {
      res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count')
      if (mongoose.connection.readyState !== 1) {
        res.status(503).json({
          msg: "Database error",
        });
      }
      else {
        next();
      }
    });
  }
}
exports.Database = Database;