const srv = require('./initialize')
const assert = require('assert')
const util = require('util')
const json = require('../missions.json')
let track1 = json.tracks[0]
let track2 = json.tracks[1]
let track1Id = ''
let authToken = ''

it('should be able to log in as that user', function (done) {
  this.timeout(5000)
  srv
    .post('/login')
    .send({"email" :'hello@vertics.co', "password" : 'lifetime'})
    .end(function (err, res)  {
      authToken = res.body.token
      res.should.have.status(200)
      done()
    })
})


describe('POST tracks from missions.json file', function () {
  json.tracks.forEach((track) => {
    it('Should return 200', function (done) {
      this.timeout(5000)
      // console.log('sending ' + track.description.name)
      srv
        .post('/tracks')
        .set('x-access-token', authToken)
        .send(track)
        .end(function (err, res) {
          res.should.have.status(200)
          done()
        })
    })
  })
})


describe('Should get 2 tracks after above test', function () {
  it('Should return 200', function (done) {
    this.timeout(5000)
    srv
        .get('/tracks')
        .end(function (err, res) {
          res.should.have.status(200)
          res.body.should.be.an('array').to.have.length(2);
          track1Id = res.body[0]._id;
          done()
        })
  })
})

  describe('Should be able to GET a track', function() {
    it('Should return 200', function (done) {
      this.timeout(5000)
      srv
        .get('/track/' + track1Id)
        .end(function (err, res) {
          res.should.have.status(200)
          res.body.description.name.should.equal('testirata')
          done()
        })
    })
  })


  describe('Should be able to UPDATE a track', function() {
    it('Should return 200', function (done) {
      this.timeout(5000)
      track1.description.name = 'Updated track description'
      srv
        .put('/track/' + track1Id)
        .set('x-access-token', authToken)
        .send(track1)
        .end(function (err, res) {
          res.should.have.status(200)
          res.body.description.name.should.equal('Updated track description')
          done()
        })
    })
  })



