const assert = require('assert')
const chai = require('chai')

const chaiHttp = require('chai-http');
const server = require('../app').default;
const should = chai.should();
chai.use(chaiHttp)
const srv = chai.request(server)
module.exports = srv
