const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require("mongoose")
const models_index = require("./app/models/index")
const morgan = require("morgan");
const methodOverride = require("method-override")
const routes = require('./config/routes')
const figaro = require("figaro");
const tokenmiddleware = require("./middlewares/tokenmiddleware")
const jwt = require('jsonwebtoken')

class App {
  constructor(NODE_ENV = "development", PORT = 3001) {
    /**
     * Setting environment for development|production
     */
    process.env.NODE_ENV = process.env.NODE_ENV || NODE_ENV;
    /**
     * Setting port number
     */
    process.env.PORT = process.env.PORT || String(PORT);

    figaro.parse("./figaro.json", function (success, err) {
      if (err) {
        console.log(err);
      }
    });
    this.app = express();
    this.app.use(cors())
    if (process.env.NODE_ENV === "development") {
      this.app.use(morgan("dev")); // log every request to the console
    }
    this.app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
    this.app.use(bodyParser.json()); // parse application/json
    this.app.use(methodOverride());

    mongoose.connect("mongodb://localhost:27017/xamk")
        .catch(err => {
          console.log("Mongo fails to connect", err.message);
        })
        .then(doc => {
          console.log("Mongo connected");
        });
    if (process.env.NODE_ENV === "development") {
      mongoose.set("debug", false);
    }
    // new tokenmiddleware.TokenMiddleware(this.app)
    new models_index.Models;
    new routes.Routes(this.app);

    this.server = this.app.listen(process.env.PORT, function () {
      console.log("The server is running in port localhost: ", process.env.PORT);
    });



    // display actual useful error messages
    this.app.use(function (err, req, res, next) {
      console.error(err.stack);
      res.status(500).send("Something broke!");
    });
  }
}
exports.App = App;
