const mongoose = require("mongoose")
const bcrypt = require("bcryptjs");

exports.userSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String
})

exports.userSchema.pre("save", async function (next) {
  try {
    if (this.isModified("password")) {
      // password hash
      const salt = await bcrypt.genSalt(10)
      this.password = await bcrypt.hash(this.password, salt)
    }
  } catch (e) {
    next (e)
  }
  next()
})

exports.User = mongoose.model("User", exports.userSchema)
