const mongoose = require("mongoose")

exports.trackSchema = new mongoose.Schema({

  description: {
    name: {
      type: String,
      required: true
    },
    qrCode: {
      type: Number,
      required: true
    }
  },
  waypoints: [
    {
      name: {
        type: String,
        required: true
      },
      description: {
        type: String,
        required: true,
      },
      image: {
        type: String,
        required: true
      },
      location: {
        latitude: {
          type: Number,
          required: true
        },
        longitude: {
          type: Number,
          required: true
        },
        radius: {
          type: Number,
          required: true
        }
      },
      question: {
        type: String,
        required: true,
      },
      options: {
        type: Array,
        required: true
      },
      correctAnswer: {
        type: Number,
        required: true
      }
    }
  ]
})


exports.trackSchema.virtual('id').get(function(){
    return this._id.toHexString();
});

exports.trackSchema.set('toJSON', {
    virtuals: true
});

exports.Track = mongoose.model("Track", exports.trackSchema)
