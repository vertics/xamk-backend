const mongoose = require('mongoose')
const util = require('util')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken');

class Auth {
  constructor (app) {
    this.app = app;
    this.User = mongoose.model("User");
  }

  routes () {



    this.app.route('/login')
      .post(async (req, res)  => {

        this.User.findOne({ email: req.body.email }, function (err, user) {
          if (err) return res.status(500).send('Error on the server.');
          if (!user) return res.status(404).send('No user found.');
          var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
          if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
          var token = jwt.sign({ id: user._id }, process.env.secret_key, {
            expiresIn: 86400 // expires in 24 hours
          });
          res.status(200).send({ auth: true, token: token });
        })
      })

    this.app.route('/logout')
      .get(function(req, res) {
        res.status(200).send({ auth: false, token: null });
    })

  }



}

exports.Auth = Auth;