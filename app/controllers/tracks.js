const mongoose = require('mongoose')
const util = require('util')

class Tracks {
  constructor(app) {
    this.app = app;
    this.Track = mongoose.model("Track");


  }

  routes() {
    this.app.route("/tracks")
      .get(async (req, res) => {
        try {
          let tracks = await this.Track.find({})
          res.setHeader("X-Total-Count", tracks.length)
          res.status(200).json(tracks)
        }
        catch (error) {
          res.status(500).json({"error": error.message})
        }
      })

    .post(async (req, res) => {
        try {
          const track = req.body;

          // create track record and save subdocuments from nested json
          let new_track = await new this.Track(track).save()
          res.status(200).json(new_track);

        } catch (err) {
          console.log(err.message)
          res.status(500).json({"error": err.message})
        }
      })


    this.app.route("/tracks/:id")
      .all((req, res) => {
        res.redirect('/track/' + req.params.id)
      })

    this.app.route("/track/:id")
      .get(async (req, res) => {
        try {

          let track = await this.Track.findOne({_id: req.params.id}).exec();

          if (track) {
            res.status(200).json(track)
          } else {
            res.status(404).json({'msg': 'Not found'})
          }
        }
        catch (error) {
          res.status(500).json({"error": error.message})
        }
      })
      .put(async (req, res) => {
          try {
            let updated_track = await this.Track.findByIdAndUpdate(req.params.id, req.body, {new: true}).exec()
            if (updated_track) {
              res.status(200).json(updated_track)
            } else {
              res.status(404).json({'msg': 'Not found'})
            }
          } catch (error) {
            res.status(500).json({"error": error.message})
          }
        })
      .delete(async (req, res) => {
        try {
          const ret = await this.Track.findByIdAndRemove(req.params.id)
          res.json(ret)
        } catch (e) {
          res.status(500).json({
            msg: e.message,
          })
        }
      })
  }


}
exports.Tracks = Tracks;
