const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const util = require('util');


class ImageUploader {
  constructor(app) {
    this.app = app;
  }

  routes() {

    this.app.use(fileUpload());

    this.app.route("/uploadimage")
      .post(async (req, res) => {
        try {
          if (!req.files) {
            return res.status(400).json({"result": "No files were uploaded."});
          }

          function uniqueid() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
              var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
              return v.toString(16);
            });
          }

          var mimetypeSplit = req.files.image.mimetype.split('/');
          var mimetype = mimetypeSplit[1];
          console.log(uniqueid() + ' _ ' + mimetype);
          var id = uniqueid();

          let sampleFile = req.files.image;
          sampleFile.mv('./uploads/' + id + '.' + mimetype, function(err) {
            if (err) {
              return res.status(500).json({"erroe": err});
            }
            var callback = {status: 'file uploaded', imageid: id + '.' + mimetype}
            res.json(callback);
          });

        } catch (err) {
          console.log(err.message)
          res.status(500).json({"error": err.message})
        }
      })

  }


}
exports.ImageUploader = ImageUploader;
